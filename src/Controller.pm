package Controller;

use strict;
use warnings;

use Network;
use threads;
use threads::shared;
use Thread::Queue;
use State;
use SrConfig;

sub new {
	my $class = shift;
	my $self = {
		_stopWork => undef,
		_ctrlQueue => new Thread::Queue(),
		_network => undef,
		_state => undef,
		_unhandledMessages => [],
		_receivingBroadcastThread => undef,
		_receivingPrivateThread => undef,
		_workThread => undef,
		_model => shift,
		_gui => shift,
		_myPlayerNumber => 0,
		_config => new SrConfig,
	};
	bless($self, $class);
	$self->initController();
	return $self;
}

sub initController {
	my $self = shift;

	my $stopWork :shared = 0;
	$self->{_stopWork} = \$stopWork;
	$self->{_gui}->ctrlQueue($self->{_ctrlQueue});

	$self->{_network} = new Network($self->{_ctrlQueue});
	$self->{_state} = new State::Initial($self);

	$self->{_receivingBroadcastThread} = threads->create(
		sub { my $net = shift; $net->startReceivingBroadcast; },
		$self->{_network})->detach;

	$self->{_receivingPrivateThread} = threads->create(
		sub { my $net = shift; $net->startReceivingPrivate; },
		$self->{_network})->detach;

# 	$self->{_guiThread} = threads->create(
# 		sub { my $gui = shift; $gui->MainLoop; },
# 		$self->{_gui});
	$self->{_workThread} = threads->create('doWork', $self);
}

sub doWork {
	my $self = shift;
	my $command;
	while(not ${$self->{_stopWork}}) {
		$command = $self->{_ctrlQueue}->dequeue();
		if($command->{type} eq 'net') {
			print 'Handling message with type: '.$command->{msg}->type."\n";
			$self->handleMessage($command->{msg}, $command->{sender});
		}
		elsif($command->{type} eq 'gui') {
			eval '$self->handle'.$command->{cmd}.'GuiCommand()';
			print $@ if($@);
		}
		else {
			print 'Not recognized command: '.$command->{type}."\n";
		}
	}
}

sub handleMessage {
	my $self = shift;
	my $message = shift;
	my $sender = shift;
	if($self->{_state}->{_class} eq 'State::Initial') {
		print 'Not started... ignoring mesage with type: '.$message->type."\n";
	}
 	elsif(eval 'defined &'.$self->{_state}->{_class}.'::handle'.$message->type.'Message') {
		eval '$self->{_state}->handle'.$message->type.'Message($message->data, $sender)';
		print $@ if($@);
	}
	else {
		print 'Not implemented handler: '.$self->{_state}->{_class}.'::handle'.$message->type.'Message'."\n";
 		push(@{$self->{_unhandledMessages}}, {msg => $message, sender => $sender});
	}
}

sub proceedUnhandledMessages {
	my $self = shift;
	my $messages = $self->{_unhandledMessages};
	$self->{_unhandledMessages} = [];
	for my $cmd (@$messages) {
		$self->handleMessage($cmd->{msg}, $cmd->{sender});
	}
}

sub createMyBoardPart {
	my $self = shift;
	$self->{_myPlayerNumber} = shift;
	my $generateTreasure = shift;

	$self->{_model}->createBoardPart($self->{_myPlayerNumber}, $generateTreasure);
}

sub createBoardPart {
	my $self = shift;
	my $playerNumber = shift;
	my $fieldsArray = shift;

	$self->{_model}->createBoardPartFromArray($playerNumber, $fieldsArray);
}

sub getMyBoardPart {
	my $self = shift;
	return $self->{_model}->{_boardParts}->{$self->{_myPlayerNumber}};
}

sub boardPartsReady {
	my $self = shift;
	$self->{_model}->joinBoardParts;
	$self->{_model}->calculateMines;
	$self->{_model}->removeFog($self->{_model}->{_players}->{$self->{_myPlayerNumber}});
	$self->postRenderEvent;
}

sub postRenderEvent {
	my $self = shift;
	my $board : shared = shared_clone($self->{_model}->copyToArray);
	my $players : shared = shared_clone($self->{_model}->getPlayers);
	my $angle : shared = shared_clone($self->{_model}->getAngleForDirection($self->{_myPlayerNumber}));

	my $maquiette : shared = shared_clone({board => $board, players => $players, angle => $angle, myPlayerNumber => $self->{_myPlayerNumber}});
	my %eventData : shared = (type => 'Maquiette', maquiette => $maquiette );
	$self->{_gui}->postEvent(\%eventData);
}

sub handleExitGuiCommand {
	my $self = shift;
	print "Exiting ...\n";
	return if(${$self->{_stopWork}} == 1);

	${$self->{_stopWork}} = 1;
	my %eventData : shared = ( type => 'Exit' );
	$self->{_gui}->postEvent(\%eventData);
}

sub handleStartGuiCommand {
	my $self = shift;
	$self->{_state}->searchGame();
	$self->{_unhandledMessages} = [];
	$self->{_model}->initModel;
}

sub handleStopGuiCommand {
	my $self = shift;
	$self->{_state} = new State::Initial($self);
}

sub handleMoveUpGuiCommand {
	my $self = shift;
	return if($self->{_state}->{_class} ne 'State::Play');
	$self->{_model}->movePlayerUp($self->{_myPlayerNumber});
	$self->postMove;
}

sub handleMoveRightGuiCommand {
	my $self = shift;
	return if($self->{_state}->{_class} ne 'State::Play');
	$self->{_model}->movePlayerRight($self->{_myPlayerNumber});
	$self->postMove;
}

sub handleMoveDownGuiCommand {
	my $self = shift;
	return if($self->{_state}->{_class} ne 'State::Play');
	$self->{_model}->movePlayerDown($self->{_myPlayerNumber});
	$self->postMove;
}

sub handleMoveLeftGuiCommand {
	my $self = shift;
	return if($self->{_state}->{_class} ne 'State::Play');
	$self->{_model}->movePlayerLeft($self->{_myPlayerNumber});
	$self->postMove;
}

sub postMove {
	my $self = shift;
	$self->{_model}->removeFog($self->{_model}->{_players}->{$self->{_myPlayerNumber}});
	$self->{_state}->sendPosition($self->{_model}->{_players}->{$self->{_myPlayerNumber}});
	if($self->{_model}->isMine($self->{_model}->{_players}->{$self->{_myPlayerNumber}})) {
		my %eventData : shared = (type => 'Mine');
		$self->{_gui}->postEvent(\%eventData);
		$self->{_model}->removeWholeFog;
	}
	elsif($self->{_model}->isTreasure($self->{_model}->{_players}->{$self->{_myPlayerNumber}})) {
		my %eventData : shared = (type => 'Won');
		$self->{_gui}->postEvent(\%eventData);
		$self->handleStopGuiCommand;
		$self->{_model}->removeWholeFog;
	}
	$self->postRenderEvent;
}

sub exit {
	my $self = shift;
	${$self->{_stopWork}} = 1;
	$self->{_ctrlQueue}->enqueue({type => 'gui', cmd => 'Exit'});
	$self->{_workThread}->join;
}

1;