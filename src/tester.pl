#!/usr/bin/perl

use strict;
use warnings;
use IO::Socket;

my $s = IO::Socket::INET->new(
				Proto => 'udp',
				LocalAddr => '0.0.0.0',
				LocalPort => 31337,
			)
			or die "Can't bind : $@\n";


my $i = 0;

while(1) {
	$s->recv(my $data, 1000, 0) or die "Can't recv: $!";
	if(int $data != $i) {
		print "Otrzymalem: $data, a chcialem: $i\n";
		$i = $data;
	}
	else {
# 		print "OK $data\n";
	}
	++$i;
}