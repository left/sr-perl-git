#!/usr/bin/perl

use strict;
use warnings;

use Google::ProtocolBuffers;

Google::ProtocolBuffers->parsefile('messages/messages.proto',
	{
	generate_code => 'messages/Messages.pm',
	create_accessors => 1,
	});
