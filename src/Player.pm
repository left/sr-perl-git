package Player;

use strict;
use warnings;

sub new {
	my $class = shift;
	my $self = {
		x => 0,
		y => 0,
		name => shift,
	};
	return bless($self, $class);
}

sub name {
	my $self = shift;
	if(@_) {
		return $self->{name} = shift;
	}
	return $self->{name};
}

sub x {
	my $self = shift;
	return $self->{x} = shift if(@_);
	return $self->{x};
}

sub y {
	my $self = shift;
	return $self->{y} = shift if(@_);
	return $self->{y};
}

1;