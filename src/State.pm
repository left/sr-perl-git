package State;

use strict;
use warnings;

sub new {
	my $class = shift;
	my $self = {
		_class => $class,
		_ctrl => shift,
	};
	bless($self, $class);
	return $self;
}

sub init {
	#empty
}

sub changeStateTo {
	my $self = shift;
	my $class = shift;
	my $state;
	eval "\$state = new $class(\$self->{_ctrl})";
	print $@ if($@);
	$self->{_ctrl}->{_state} = $state;

	print 'State changed from: '.$self->{_class}.' to: '.$class."\n";

	$self->{_ctrl}->{_state}->init();
	$self->{_ctrl}->proceedUnhandledMessages();
}

package State::Initial;

use strict;
use warnings;
use base 'State';

sub searchGame {
	my $self = shift;
	$self->changeStateTo('State::WaitForPlayers');
}

package State::WaitForPlayers;

use strict;
use warnings;
use base 'State';

sub init {
	my $self = shift;
	$self->{_ctrl}->{_model}->initModel;
	my $ipTable = $self->{_ctrl}->{_network}->{_ipTable};
	$ipTable->clear;

	my $myaddr = $self->{_ctrl}->{_network}->{_localAddr};
	$self->addToIpTable($myaddr);
	$self->sendGameSearchRequest;
}

sub sendGameSearchRequest {
	my $self = shift;
	my $m = new GameSearchRequest();
	$self->{_ctrl}->{_network}->sendBrodcastMessage($m);
}

sub sendGameSearchResponse {
	my $self = shift;
	my $recivier = shift;
	my $m = new GameSearchResponse();
	$self->{_ctrl}->{_network}->sendToMessage($m, $recivier);
}

sub handleGameSearchRequestMessage {
	my $self = shift;
	my $message = GameSearchRequest->decode(shift);
	my $sender = shift;
	my $myaddr = $self->{_ctrl}->{_network}->{_localAddr};

	if($sender ne $myaddr) {
		$self->sendGameSearchResponse($sender);
		$self->addToIpTable($sender);
	}
}

sub handleGameSearchResponseMessage {
	my $self = shift;
	my $message = GameSearchResponse->decode(shift);
	my $sender = shift;
	$self->addToIpTable($sender);
}

sub addToIpTable {
	my $self = shift;
	my $sender = shift;
	my $ipTable = $self->{_ctrl}->{_network}->{_ipTable};
	$ipTable->insert($sender) and print $sender." was added to ip table\n";
# 	if($ipTable->size == 4) {
	if($ipTable->size == $self->{_ctrl}->{_config}->{NUMBER_OF_PLAYERS}) {
		$self->changeStateTo('State::CheckIpTables');
	}
}

package State::CheckIpTables;

use strict;
use warnings;
use List::Util 'shuffle';
use base 'State';

sub init {
	my $self = shift;
	$self->{_consistentSenders} = Set::Scalar->new();
	$self->sendIpTables;
}

sub sendIpTables {
	my $self = shift;
	my $ipTable = $self->{_ctrl}->{_network}->{_ipTable};

	my @addresses;
	while(defined(my $addr = $ipTable->each)) {
		my ($ip, $port) = split(':', $addr);
		push(@addresses, { ip => $ip, port => $port });
	}
	my $m = new IpTable({ addresses => \@addresses });
	$self->{_ctrl}->{_network}->sendEveryOthersMessage($m);
}

sub handleIpTableMessage {
	my $self = shift;
	my $message = IpTable->decode(shift);
	my $sender = shift;

	my $addresses = Set::Scalar->new();
	foreach(@{$message->addresses}) {
		$addresses->insert($_->{ip}.':'.$_->{port});
	}

	my $ipTable = $self->{_ctrl}->{_network}->{_ipTable};
	my $localAddr = $self->{_ctrl}->{_network}->{_localAddr};

	# TODO this is not good condition
	if(!($ipTable == $addresses)) {
		$self->changeStateTo('State::WaitForPlayers');
		return;
	}

	$self->{_consistentSenders}->insert($sender);
	my $others = $ipTable->difference(Set::Scalar->new($localAddr));

	if($self->{_consistentSenders} == $others) {
		if($self->checkIamCoordinator($ipTable, $localAddr)) {
			print "I am the coordinator.\n";
			$self->doCoordinatorWork;
			$self->changeStateTo('State::ShareBoardParts');
		}
		else {
			$self->changeStateTo('State::WaitForBoardParams');
		}
	}
}

sub doCoordinatorWork {
	my $self = shift;
	my $playerWithTreasure = int rand($self->{_ctrl}->{_config}->{NUMBER_OF_PLAYERS}) + 1;
	my @players = shuffle((1..$self->{_ctrl}->{_config}->{NUMBER_OF_PLAYERS}));

	my $ipTable = $self->{_ctrl}->{_network}->{_ipTable};
	my $localAddr = $self->{_ctrl}->{_network}->{_localAddr};
	my $others = $ipTable->difference(Set::Scalar->new($localAddr));

	for my $player ($others->members) {
		my $boardParams = $self->createBoardParams(pop(@players), $playerWithTreasure);
		$self->{_ctrl}->{_network}->sendToMessage($boardParams, $player);
	}
	$self->{_ctrl}->createMyBoardPart($players[0], $players[0] == $playerWithTreasure);
}

sub checkIamCoordinator {
	my $self = shift;
	my $ipTable = shift;
	my $myAddr = shift;
	my $min = (sort $ipTable->members)[0];
	return ($myAddr eq $min);
}

sub createBoardParams {
	my $self = shift;
	my $player = shift;
	my $playerWithTreasure = shift;
	my $hasTreasure = $playerWithTreasure == $player ? 1 : 0;
	return new BoardParams({ playerNumber => $player, hasTreasure => $hasTreasure });
}

package State::WaitForBoardParams;

use strict;
use warnings;
use base 'State';

sub handleBoardParamsMessage {
	my $self = shift;
	my $message = BoardParams->decode(shift);
	my $sender = shift;

	$self->{_ctrl}->createMyBoardPart($message->playerNumber, $message->hasTreasure);
	$self->changeStateTo('State::ShareBoardParts');
}

package State::ShareBoardParts;

use strict;
use warnings;
use base 'State';

sub init {
	my $self = shift;
	$self->sendMyBoardPart;
}

sub sendMyBoardPart {
	my $self = shift;
	my @fields;
	my $boardPart = $self->{_ctrl}->getMyBoardPart();
	
	for my $x (0..($boardPart->size - 1)) {
		for my $y (0..($boardPart->size - 1)) {
			push(@fields, { x => $x, y => $y, type => $boardPart->elem($x, $y) });
		}
	}

	print "My player number: ".$self->{_ctrl}->{_myPlayerNumber}."\n";
	my $m = new BoardPart({playerNumber => $self->{_ctrl}->{_myPlayerNumber}, fields => \@fields});
	$self->{_ctrl}->{_network}->sendEveryOthersMessage($m);
}

sub handleBoardPartMessage {
	my $self = shift;
	my $message = BoardPart->decode(shift);
	my $sender = shift;

	$self->{_ctrl}->createBoardPart($message->playerNumber, $message->fields);
	if(scalar keys %{$self->{_ctrl}->{_model}->{_boardParts}} == $self->{_ctrl}->{_config}->{NUMBER_OF_PLAYERS}) {
		$self->{_ctrl}->boardPartsReady;
		$self->changeStateTo('State::Play');
	}
}

package State::Play;

use strict;
use warnings;
use base 'State';

sub sendPosition {
	my $self = shift;
	my $position = shift;
	my $m = new PositionPlayer({
		x => $position->{x},
		y => $position->{y},
		playerNumber => $self->{_ctrl}->{_myPlayerNumber},
	});
	$self->{_ctrl}->{_network}->sendEveryOthersMessage($m);
}

sub handlePositionPlayerMessage {
	my $self = shift;
	my $message = PositionPlayer->decode(shift);
	my $sender = shift;

	my $position = {x => $message->x, y => $message->y};
	$self->{_ctrl}->{_model}->movePlayer($message->playerNumber, $position);
	if($self->{_ctrl}->{_model}->isTreasure($self->{_ctrl}->{_model}->{_players}->{$message->playerNumber})) {
		my %eventData : shared = (type => 'Lost');
		$self->{_ctrl}->{_gui}->postEvent(\%eventData);
		$self->{_ctrl}->handleStopGuiCommand;
		$self->{_ctrl}->{_model}->removeWholeFog;
	}
	$self->{_ctrl}->postRenderEvent;
}

1;
