#!/usr/bin/perl

use strict;
use warnings;

use messages::Messages;
use Controller;
use Gui;
use Model;

my $gui = Gui->new();
my $model = new Model;
my $ctrl = new Controller($model, $gui);
# $ctrl->doWork;
$gui->MainLoop;
$ctrl->exit;
