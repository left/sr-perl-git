package Network;

use strict;
use warnings;

use IO::Socket;
use IO::Interface::Simple;
use Set::Scalar;
use SrConfig;

sub new {
	my $class = shift;
	my $self = {
		_ctrlQueue => shift,
		_ipTable => Set::Scalar->new(),
		_ip => undef,
		_port => undef,
		_localAddr => undef,
		_config => new SrConfig,
	};
	bless($self, $class);
	$self->initNetwork();
	return $self;
}

sub initNetwork {
	my $self = shift;
	my $interface = IO::Interface::Simple->new($self->{_config}->{INTERFACE});
	$self->{_ip} = $interface->address;

	$self->{_socket} = IO::Socket::INET->new(
				Proto => 'udp',
#  				LocalAddr => $self->{_ip},
				LocalAddr => '0.0.0.0',
				LocalPort => $self->{_config}->{BROADCAST_PORT},
				Reuse => 1,
				Broadcast => 1,
				)
			or die "Can't bind : $@\n";

	$self->{_game_socket} = IO::Socket::INET->new(
				Proto => 'udp',
				LocalAddr => $self->{_ip},
 				Broadcast => 1,
				)
			or die "Can't bind : $@\n";

	$self->{_port} = $self->{_game_socket}->sockport;
	$self->{_localAddr} = $self->{_ip}.':'.$self->{_port};
}

sub sendBroadcast {
	my $self = shift;
	my $message = shift;
	my $portaddr = sockaddr_in($self->{_config}->{BROADCAST_PORT}, INADDR_BROADCAST);
	$self->{_game_socket}->send($message, 0, $portaddr) or die "Can't send broadcast: $@\n";
}

sub sendTo {
	my $self = shift;
	my $message = shift;
	my $addr = inet_aton(shift);
	my $port = shift;
	my $portaddr = sockaddr_in($port, $addr);
	$self->{_game_socket}->send($message, 0, $portaddr) or die "Can't send to: $@\n";
}

sub sendBrodcastMessage {
	my $self = shift;
	my $message = shift;
	my $type = ref $message;
	my $data = $message->encode;
	print "Sending ".$type." message on broadcast\n";
	my $wrap = new Message({type => $type, data => $data});
	$self->sendBroadcast($wrap->encode);
}

sub sendToMessage {
	my $self = shift;
	my $message = shift;
	my ($addr, $port) = split(':', shift);

	my $type = ref $message;
	my $data = $message->encode;
	print "Sending ".$type." message to: $addr:$port\n";
	my $wrap = new Message({type => $type, data => $data});
	$self->sendTo($wrap->encode, $addr, $port);
}

sub sendEveryOthersMessage {
	my $self = shift;
	my $message = shift;

	my $type = ref $message;
	my $data = $message->encode;

	my $wrap = new Message({type => $type, data => $data});
	my $encoded = $wrap->encode;
	my $others = $self->{_ipTable}->difference(Set::Scalar->new($self->{_localAddr}));

	while (defined(my $a = $others->each)) {
		my ($addr, $port) = split(':', $a);
		print "Sending ".$type." message to: $addr:$port\n";
		$self->sendTo($encoded, $addr, $port);
	}
}

sub recv {
	my $self = shift;
	my $socket = shift;
	my $portaddr = $socket->recv(my $data, $self->{_config}->{RECV_BUFFER_LEN}, 0) or die "Can't recv: $!";
	my ($port, $addr) = sockaddr_in($portaddr);
	my $host = inet_ntoa($addr);
	my $message = Message->decode($data) or die "Can't parse message: $!";
	return ($message, $host, $port);
}

sub recvBroadcast {
	my $self = shift;
	return $self->recv($self->{_socket});
}

sub recvPrivate {
	my $self = shift;
	return $self->recv($self->{_game_socket});
}

sub startReceivingBroadcast {
	my $self = shift;
	print "Starting receiving broadcast...\n";
	while (1) {
		my ($message, $host, $port) = $self->recvBroadcast();
		print 'Reveived broadcast msg with type: '.$message->type;
		my $sender = $host.':'.$port;
		print ' ('.$sender.")\n";
		$self->dispatch($message, $sender) if($sender ne $self->{_localAddr});
	}
}

sub startReceivingPrivate {
	my $self = shift;
	print "Starting receiving private...\n";
	while (1) {
		my ($message, $host, $port) = $self->recvPrivate();
		print 'Received private msg with type: '.$message->type;
		my $sender = $host.':'.$port;
		print ' ('.$sender.")\n";
		$self->dispatch($message, $sender);
	}
}

sub dispatch {
	my $self = shift;
	my $message = shift;
	my $sender = shift;
	my $command = { type => 'net', msg => $message, sender => $sender };
	$self->{_ctrlQueue}->enqueue($command);
}

1;