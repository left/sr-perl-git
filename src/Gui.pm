package Gui;

use strict;

use threads;
use threads::shared;
use Wx;
use base 'Wx::App';

use Wx qw(WXK_UP WXK_RIGHT WXK_DOWN WXK_LEFT);
use Wx::Event qw(EVT_KEY_DOWN);

my $CUSTOM_EVENT : shared = Wx::NewEventType;

sub OnInit {
	my $self = shift;
	$self->{_filenamesAndIndexes} = {
		empty => [0, '../img/empty.png'],
		1 => [1, '../img/1.png'],
		2 => [2, '../img/2.png'],
		3 => [3, '../img/3.png'],
		4 => [4, '../img/4.png'],
		5 => [5, '../img/5.png'],
		6 => [6, '../img/6.png'],
		7 => [7, '../img/7.png'],
		8 => [8, '../img/8.png'],
		block => [9, '../img/block.png'],
		mine => [10, '../img/empty.png'],
		treasure => [11, '../img/treasure.png'],
		hidden => [12, '../img/hidden.png'],
		player1 => ['player1', '../img/monkey.png'],
		player2 => ['player2', '../img/angel.png'],
		player3 => ['player3', '../img/devil.png'],
		player4 => ['player4', '../img/glasses.png'],
		compass => ['compass', '../img/arrow.png'],
	};

	my $frame = SrFrame->new($self, undef, -1, "SR Projekt", [0, 0], [640, 480]);
	$self->{_frame} = $frame;

	EVT_KEY_DOWN( $self, \&onKeyPressed );

	$frame->Show(1);
	return 1;
}

sub ctrlQueue {
	my $self = shift;
	if(@_) {
		return $self->{_queue} = shift;
	}
	return $self->{_queue};
}

sub invokeCommand {
	my $self = shift;
	my $cmd = shift;
	$self->{_queue}->enqueue({ type => 'gui', cmd => $cmd });
}

sub postEvent {
	my $self = shift;
	my $event = shift;
	my $threvent = new Wx::PlThreadEvent( -1, $CUSTOM_EVENT, $event );
	Wx::PostEvent( $self->{_frame}, $threvent );
}


sub onKeyPressed {
	my $self = shift;
	my $event = shift;

	if($event->GetKeyCode == WXK_UP) {
		$self->invokeCommand('MoveUp');
	}
	elsif($event->GetKeyCode == WXK_RIGHT) {
		$self->invokeCommand('MoveRight');
	}
	elsif($event->GetKeyCode == WXK_DOWN) {
		$self->invokeCommand('MoveDown');
	}
	elsif($event->GetKeyCode == WXK_LEFT) {
		$self->invokeCommand('MoveLeft');
	}
	else {
		$event->Skip();
	}
}

package SrFrame;

use strict;

use threads;
use threads::shared;
use Wx;
use base 'Wx::Frame';

use Wx qw(:id);
use Wx qw(wxVERTICAL wxHORIZONTAL wxEXPAND wxALL wxOK wxSHAPED);

use Wx::Event qw(EVT_SIZE
		 EVT_MENU
		 EVT_COMBOBOX
		 EVT_UPDATE_UI
		 EVT_TOOL_ENTER
		 EVT_COMMAND
);



sub new {
	my $class = shift;
	my $app = shift;
	my $this = $class->SUPER::new(@_);
	$this->{_app} = $app;
	my ($IDM_FILE_START, $IDM_FILE_STOP) = (10000 .. 10100);
	my $file_menu = Wx::Menu->new();
	my @file_menu_entries = ([$IDM_FILE_START,"&Start\tCtrl-S","Start" ],
				[$IDM_FILE_STOP,"Sto&p\tCtrl-P","Stop" ],
				['-'],
				[wxID_EXIT, "E&xit\tCtrl-X", "Exit $0"]
	);

	foreach (@file_menu_entries) {
		if ($$_[0] eq '-') {
			$file_menu->AppendSeparator();
		} else {
			$file_menu->Append ($$_[0], $$_[1], $$_[2]);
		}
	}

	my $menubar = Wx::MenuBar->new();
	$menubar->Append($file_menu, '&File');
	$this->SetMenuBar($menubar);

	my $panel = new BoardPanel($this);
	$this->{_panel} = $panel;
	my $sidePanel = SidePanel->new($this, -1, [-1, -1], [70, -1]);
	$this->{_sidePanel} = $sidePanel;

	my $sizer = Wx::BoxSizer->new(wxHORIZONTAL);
	$sizer->Add($panel, 1, wxEXPAND);
	$sizer->Add($sidePanel, 0, wxEXPAND);
	$this->SetSizer($sizer);

	EVT_MENU( $this, wxID_EXIT, \&exitCmd );
 	EVT_MENU( $this, $IDM_FILE_START, \&startCmd );
	EVT_MENU( $this, $IDM_FILE_STOP, \&stopCmd );
	EVT_COMMAND( $this, -1, $CUSTOM_EVENT, \&onCustomEvent );

	return $this;
}

sub exitCmd {
	my $self = shift;
	$self->{_app}->invokeCommand('Exit');
}

sub startCmd {
	my $self = shift;
	$self->{_app}->invokeCommand('Start');
}

sub stopCmd {
	my $self = shift;
	$self->{_app}->invokeCommand('Stop');
}

sub onCustomEvent {
	my $self = shift;
	my $event = shift;
	my $data = $event->GetData;

	# TODO dispacher for this?
	if($data->{type} eq 'Exit') {
		$self->Close(1);
	}
	elsif($data->{type} eq 'Maquiette') {
		$self->{_panel}->maquiette($data->{maquiette});
		$self->{_sidePanel}->maquiette($data->{maquiette});
		$self->{_panel}->paintNow;
		$self->{_sidePanel}->paintNow;
	}
	elsif($data->{type} eq 'Won') {
		my $dialog = Wx::MessageDialog->new($self, "You are the WINNER!", "Winner", wxOK);
		$dialog->ShowModal;
	}
	elsif($data->{type} eq 'Mine') {
		my $dialog = Wx::MessageDialog->new($self, "You owned yourself by standing on mine!", "Looser", wxOK);
		$dialog->ShowModal;
	}
	elsif($data->{type} eq 'Lost') {
		my $dialog = Wx::MessageDialog->new($self, "You lost! Somebody has been faster than you.", "Looser", wxOK);
		$dialog->ShowModal;
	}
	
}

package BoardPanel;

use strict;
use warnings;

use threads;
use threads::shared;
use Wx;
use base 'Wx::Panel';

use Wx qw(:id);
use Wx qw(wxWHITE);
use IO::File;
use POSIX qw(floor ceil);

use Wx::Event qw(EVT_SIZE
		 EVT_MENU
		 EVT_COMBOBOX
		 EVT_UPDATE_UI
		 EVT_TOOL_ENTER
		 EVT_COMMAND
		 EVT_PAINT
		 EVT_SIZE
);

sub new {
	my $class = shift;
	my $this = $class->SUPER::new(@_);

	$this->{_filenamesAndIndexes} = $this->GetParent()->{_app}->{_filenamesAndIndexes};
	$this->{_withBackground} = '(11)';

	$this->{_fieldWidth} = 0;
	$this->{_fieldHeight} = 0;
	$this->{_images} = {};
	$this->{_bitmaps} = {};

	$this->{_maquiette} = undef;

	$this->SetBackgroundColour( wxWHITE );

	$this->loadImages;

	EVT_PAINT( $this, \&OnPaint );
	EVT_SIZE( $this, \&OnSize );

	return $this;
}

sub OnSize {
	my ($self, $event) = @_;
	@_ = ();
	$self->{_hackSize} = $event->GetSize();
	$self->Refresh();
	$event->Skip();
}

sub maquiette {
	my $self = shift;
	if(@_) {
		$self->{_maquiette} = shift;
	}
	return $self->{_maquiette};
}

sub OnPaint {
	my ($self, $event) = @_;
	@_ = ();
	my $dc = Wx::PaintDC->new($self);
	$self->render($dc);
}

sub paintNow {
	my ($self) = @_;
	my $clientDc = Wx::ClientDC->new($self);
	my $dc = Wx::BufferedDC->new($clientDc, $clientDc->GetSize());
	$self->render($dc);
}

sub render {
	my ($self, $dc) = @_;
	if(!$self->{_maquiette}) {
		return;
	}

	my $board = $self->{_maquiette}->{board};
	my $boardSize = scalar @{$board};

	my $newW = $self->{_hackSize}->x;
	my $newH = $self->{_hackSize}->y;

	my $fieldWidth = $newW / $boardSize;
	my $fieldHeight = $newH / $boardSize;

	if($self->{_fieldWidth} != $fieldWidth or $self->{_fieldHeight} != $fieldHeight) {
		$self->{_fieldWidth} = $fieldWidth;
		$self->{_fieldHeight} = $fieldHeight;
		$self->resizeAllImages;
	}

	my ($posX, $posY) = (0, 0);

	for my $i (0..$boardSize-1) {
		for my $j (0..$boardSize-1) {
			$self->drawField($dc, $board->[$j]->[$i], $posX, $posY);
			$posX += $self->{_fieldWidth};
		}
		$posY += $self->{_fieldHeight};
		$posX = 0;
	}

	my $players = $self->{_maquiette}->{players};
	while(my ($key, $player) = each(%$players)) {
		$self->drawPlayer($dc, $player, $key) if($player->{alive});
	}
}

sub drawField {
	my ($self, $dc, $fieldType, $posX, $posY) = @_;

	if($fieldType =~ $self->{_withBackground}) {
		$dc->DrawBitmap($self->{_bitmaps}->{0} , floor($posX), floor($posY), 0);
	}
	$dc->DrawBitmap($self->{_bitmaps}->{$fieldType} , floor($posX), floor($posY), 0);
}

sub drawPlayer {
	my ($self, $dc, $player, $number) = @_;
	$dc->DrawBitmap($self->{_bitmaps}->{'player'.$number} , floor($player->{x} * $self->{_fieldWidth}), floor($player->{y} * $self->{_fieldHeight}), 0);
}

sub resizeAllImages {
	my $self = shift;

	while(my ($i, $img) = each(%{$self->{_images}})) {
		$self->{_bitmaps}->{$i} = Wx::Bitmap->new($img->Scale(ceil($self->{_fieldWidth}), ceil($self->{_fieldHeight})));
	}
}

sub loadImages {
	my $self = shift;
	my $handler = Wx::PNGHandler->new();

	while(my ($key, $fileInd) = each(%{$self->{_filenamesAndIndexes}})) {
		my $file = IO::File->new($fileInd->[1] , "r" );
		unless ($file) { print "Can't load ".$fileInd->[1]; return undef };
		binmode $file;

		my $image = Wx::Image->new();
		$handler->LoadFile( $image, $file );
		$self->{_images}->{$fileInd->[0]} = $image;
	}
}

package SidePanel;

use strict;
use warnings;

use threads;
use threads::shared;
use Wx;
use base 'Wx::Panel';

use Wx qw(:id);
use Wx qw(wxWHITE wxSOLID);
use IO::File;
use POSIX qw(floor ceil);

use Wx::Event qw(EVT_UPDATE_UI
		 EVT_PAINT
		 EVT_SIZE
);

sub new {
	my $class = shift;
	my $this = $class->SUPER::new(@_);

	$this->{_filenamesAndIndexes} = $this->GetParent()->{_app}->{_filenamesAndIndexes};

	$this->{_images} = {};
	$this->{_bitmaps} = {};
	$this->{_maquiette} = undef;
	$this->SetBackgroundColour( wxWHITE );

	$this->loadImages;

	EVT_PAINT( $this, \&OnPaint );
	EVT_SIZE( $this, \&OnSize );

	return $this;
}

sub maquiette {
	my $self = shift;
	if(@_) {
		$self->{_maquiette} = shift;
	}
	return $self->{_maquiette};
}

sub OnPaint {
	my ($self, $event) = @_;
	@_ = ();
	my $dc = Wx::PaintDC->new($self);
	$self->render($dc);
}


sub OnSize {
	my ($self, $event) = @_;
	@_ = ();
	$self->Refresh();
	$event->Skip();
}

sub paintNow {
	my ($self) = @_;
	my $dc = Wx::ClientDC->new($self);
	$self->render($dc);
}

sub render {
	my ($self, $dc) = @_;
	if(!$self->{_maquiette}) {
		return;
	}

	$dc->SetBackground(Wx::Brush->new(wxWHITE, wxSOLID));
	$dc->Clear;

	my $image = $self->{_images}->{compass};
	my $center = Wx::Point->new($image->GetWidth() / 2, $image->GetHeight() / 2);
	$image = $self->{_images}->{compass}->Rotate($self->{_maquiette}->{angle}, $center);
	my $compass = Wx::Bitmap->new($image);

	$dc->DrawBitmap($compass,
			$self->GetSize()->x / 2 - $image->GetWidth / 2,
			(($self->GetSize()->y) / 4) - $image->GetHeight / 2,
			0);

	$image = $self->{_images}->{'player'.$self->{_maquiette}->{myPlayerNumber}};
	my $player = Wx::Bitmap->new($image);

	$dc->DrawBitmap($player,
			$self->GetSize()->x / 2 - $image->GetWidth / 2,
			(($self->GetSize()->y) / 4) * 3 - $image->GetHeight / 2,
			0);
}

sub loadImages {
	my $self = shift;
	my $handler = Wx::PNGHandler->new();

	my $maxSize = $self->GetSize()->x;
	my @files = ($self->{_filenamesAndIndexes}->{compass} );
	for my $i (1..4) {
		push(@files, $self->{_filenamesAndIndexes}->{'player'.$i});
	}

	for my $fileInd (@files)  {
		my $file = IO::File->new($fileInd->[1] , "r" );
		unless ($file) { print "Can't load ".$fileInd->[1]; return undef };
		binmode $file;

		my $image = Wx::Image->new();
		$handler->LoadFile( $image, $file );
		$image = $image->Scale($maxSize, $maxSize);
		$self->{_images}->{$fileInd->[0]} = $image;
	}
}

1;