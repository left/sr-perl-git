package Model;

use strict;
use warnings;

use Board;
use SrConfig;
use POSIX qw(floor);
use Math::Trig;
use Math::Trig ':pi';

sub new {
	my $class = shift;
	my $self = {
		_board => undef,
		_boardParts => undef,
		_fog => undef,
		_players => {},
		_config => new SrConfig,
		_treasure => undef,
	};
	bless($self, $class);
# 	$self->initModel;
	return $self;
}

sub initModel {
	my $self = shift;
	$self->{_boardParts} = {};
	$self->{_board} = new Board($self->{_config}->{BOARD_SIZE});
	$self->{_fog} = new Board($self->{_config}->{BOARD_SIZE});
	$self->{_fog}->initBoard($self->{_fog}->{_fieldTypes}->{FOG});
	$self->putPlayers;
}
 
sub createBoardPart {
	my $self = shift;
	my $playerNumber = shift;
	my $hasTreasure = shift;
	my $empty = {
		x => floor(($self->{_players}->{$playerNumber}->{x}) / 2),
		y => floor(($self->{_players}->{$playerNumber}->{y}) / 2),
	};

	my $partSize = $self->{_config}->{BOARD_SIZE} / 2;
	my $boardPart = new Board($partSize);
	$boardPart->elem($empty->{x}, $empty->{y}, $boardPart->{_fieldTypes}->{BLOCK});
	$boardPart->generateBlocks($self->{_config}->{BLOCKS_NUM});
	$boardPart->generateMines($self->{_config}->{MINES_NUM});
	if($hasTreasure) {
		$boardPart->generateTreasure;
	}
	$boardPart->elem($empty->{x}, $empty->{y}, $boardPart->{_fieldTypes}->{EMPTY});
	$self->{_boardParts}->{$playerNumber} = $boardPart;
}

sub createBoardPartFromArray {
	my $self = shift;
	my $playerNumber = shift;
	my $fieldsArray = shift;

	my $partSize = $self->{_config}->{BOARD_SIZE} / 2;
	my $boardPart = new Board($partSize);
	foreach(@{$fieldsArray}) {
		$boardPart->elem($_->{x}, $_->{y}, $_->{type});
	}
	$self->{_boardParts}->{$playerNumber} = $boardPart;
}

sub joinBoardParts {
	my $self = shift;
	my @funs;
	$funs[1] = sub { return (shift, shift); };
	$funs[2] = sub { return ($self->{_board}->size / 2 + shift, shift); };
	$funs[3] = sub { return (shift, $self->{_board}->size / 2 + shift); };
	$funs[4] = sub { return ($self->{_board}->size / 2 + shift, $self->{_board}->size / 2 + shift); };

	while(my ($playerNumber, $part) = each(%{$self->{_boardParts}})) {
		for my $x (0..($part->size - 1)) {
			for my $y (0..($part->size - 1)) {
				my ($tx, $ty) = $funs[$playerNumber]->($x, $y);
				$self->{_board}->elem($tx, $ty, $part->elem($x, $y));
			}
		}
	}
# 	$self->{_board}->dprint;
}

sub calculateMines {
	my $self = shift;
	my $board = $self->{_board};
	for my $x (0..($board->size - 1)) {
		for my $y (0..($board->size - 1)) {
			if($board->elem($x, $y) == $board->{_fieldTypes}->{EMPTY}) {
				my $count = $board->getMinesCountAround($x, $y);
				$board->elem($x, $y, $count);
			}
			elsif($board->elem($x, $y) == $board->{_fieldTypes}->{TREASURE}) {
				$self->{_treasure} = {x => $x, y => $y};
			}
		}
	}
}

sub putPlayers {
	my $self = shift;
	my $size = $self->{_board}->size;
	$self->{_players}->{1} = {x => 0, y => 0, alive => 1};
	$self->{_players}->{2} = {x => $size - 1, y => 0, alive => 1};
	$self->{_players}->{3} = {x => 0, y => $size - 1, alive => 1};
	$self->{_players}->{4} = {x => $size - 1, y => $size - 1, alive => 1};
}

sub movePlayerUp {
	my $self = shift;
	my $playerNumber = shift;
	my $newPosition = {
		x => $self->{_players}->{$playerNumber}->{x},
		y => ($self->{_players}->{$playerNumber}->{y} - 1) % $self->{_board}->size,
	};
	$self->movePlayer($playerNumber, $newPosition);
}

sub movePlayerRight {
	my $self = shift;
	my $playerNumber = shift;
	my $newPosition = {
		x => ($self->{_players}->{$playerNumber}->{x} + 1) % $self->{_board}->size,
		y => $self->{_players}->{$playerNumber}->{y},
	};
	$self->movePlayer($playerNumber, $newPosition);
}

sub movePlayerDown {
	my $self = shift;
	my $playerNumber = shift;
	my $newPosition = {
		x => $self->{_players}->{$playerNumber}->{x},
		y => ($self->{_players}->{$playerNumber}->{y} + 1) % $self->{_board}->size,
	};
	$self->movePlayer($playerNumber, $newPosition);
}

sub movePlayerLeft {
	my $self = shift;
	my $playerNumber = shift;
	my $newPosition = {
		x => ($self->{_players}->{$playerNumber}->{x} - 1) % $self->{_board}->size,
		y => $self->{_players}->{$playerNumber}->{y},
	};
	$self->movePlayer($playerNumber, $newPosition);
}

sub movePlayer {
	my $self = shift;
	my $playerNumber = shift;
	my $newPosition = shift;

	return if(!$self->{_players}->{$playerNumber}->{alive}
		  or !$self->canStand($newPosition));

	$self->{_players}->{$playerNumber}->{alive} = 0 if($self->isMine($newPosition));

	$self->{_players}->{$playerNumber}->{x} = $newPosition->{x};
	$self->{_players}->{$playerNumber}->{y} = $newPosition->{y};
}

sub canStand {
	my $self = shift;
	my $position = shift;

	return $self->{_board}->elem($position->{x}, $position->{y}) != $self->{_board}->{_fieldTypes}->{BLOCK};
}

sub isMine {
	my $self = shift;
	my $position = shift;

	return $self->{_board}->elem($position->{x}, $position->{y}) == $self->{_board}->{_fieldTypes}->{MINE};
}

sub isTreasure {
	my $self = shift;
	my $position = shift;

	return $self->{_board}->elem($position->{x}, $position->{y}) == $self->{_board}->{_fieldTypes}->{TREASURE};
}

sub isFog {
	my $self = shift;
	my $position = shift;

	return $self->{_fog}->elem($position->{x}, $position->{y}) == $self->{_board}->{_fieldTypes}->{FOG};
}

sub removeFog {
	my $self = shift;
	my $position = shift;
	my $size = $self->{_board}->size;
	my $by = ($position->{y} - 1) % $size;
	my $ty = ($position->{y} + 1) % $size;
	my $lx = ($position->{x} - 1) % $size;
	my $rx = ($position->{x} + 1) % $size;

	my $val = $self->{_board}->{_fieldTypes}->{EMPTY};
	$self->{_fog}->elem($position->{x}, $position->{y}, $val);
	$self->{_fog}->elem($position->{x}, $ty, $val);
	$self->{_fog}->elem($rx, $ty, $val);
	$self->{_fog}->elem($rx, $position->{y}, $val);
	$self->{_fog}->elem($rx, $by, $val);
	$self->{_fog}->elem($position->{x}, $by, $val);
	$self->{_fog}->elem($lx, $by, $val);
	$self->{_fog}->elem($lx, $position->{y}, $val);
	$self->{_fog}->elem($lx, $ty, $val);
}

sub removeWholeFog {
	my $self = shift;
	$self->{_fog}->initBoard($self->{_fog}->{_fieldTypes}->{EMPTY});
}

sub copyToArray {
	my $self = shift;
	return $self->{_board}->copyToArray($self->{_fog});
}

sub getPlayers {
	my $self = shift;
	my $players = {};
	while(my ($key, $player) = each(%{$self->{_players}})) {
		my ($x, $y) = ($player->{x}, $player->{y});
		if($self->isFog($player)) {
			($x, $y) = ($self->{_board}->size, $self->{_board}->size);
		}
		$players->{$key} = {
			x => $x,
			y => $y,
			alive => $player->{alive},
		};
	}
	return $players
}

sub getAngleForDirection {
	my $self = shift;
	my $num = shift;
	my $player = $self->{_players}->{$num};

	my $x = $player->{x} - $self->{_treasure}->{x};
	my $y = $player->{y} - $self->{_treasure}->{y};
	my $ret = 0;

	if($x == 0) {
		$ret = $y > 0 ? 0 : pi;
	}
	else {
		$ret = pip2 - atan($y / $x);
		$ret += pi if($x < 0);
	}

	return $ret
}

1;