package SrConfig;

use strict;
use warnings;

sub new {
	my $class = shift;
	my $self = {
		NUMBER_OF_PLAYERS => 2,
		BROADCAST_PORT => 31337,
		RECV_BUFFER_LEN => 1000,
		BOARD_SIZE => 20,
		BLOCKS_NUM => 10,
		MINES_NUM => 10,
		INTERFACE => 'lo',
	};
	bless($self, $class);
	return $self;
}

1;