package Board;

use strict;
use warnings;

sub new {
	my $class = shift;
	my $size = shift;
	my $self = {
		_size => $size,
		_board => undef,
		_fieldTypes => { EMPTY => 0, BLOCK => 9, MINE => 10, TREASURE => 11, FOG => 12},
	};
	bless($self, $class);
	$self->initBoard($self->{_fieldTypes}->{EMPTY});
	return $self;
}

sub initBoard {
	my $self = shift;
	my $init = shift;
	$self->{_board} = [];
	for(1..$self->{_size}) {
		my $array = [];
		push(@$array, $init) for(1..$self->{_size});
		push(@{$self->{_board}}, $array);
	}
}

sub size {
	my $self = shift;
	if(@_) {
		return $self->{_size} = shift;
	}
	return $self->{_size};
}

sub board {
	my $self = shift;
	if(@_) {
		return $self->{_board} = shift;
	}
	return $self->{_board};
}

sub elem {
	my $self = shift;
	my $x = shift;
	my $y = shift;
	if(@_) {
		$self->{_board}->[$x][$y] = shift;
	}
	return $self->{_board}->[$x][$y];
}

sub generateBlocks {
	my $self = shift;
	my $count = shift;

	for my $i (0..$count-1) {
		my ($x, $y) = $self->emptyField;
		$self->elem($x, $y, $self->{_fieldTypes}->{BLOCK});
	}
}

sub generateMines {
	my $self = shift;
	my $count = shift;

	for my $i (0..$count-1) {
		my ($x, $y) = $self->emptyField;
		$self->elem($x, $y, $self->{_fieldTypes}->{MINE});
	}
}

sub generateTreasure {
	my $self = shift;

	my ($x, $y) = $self->emptyField;
	$self->elem($x, $y, $self->{_fieldTypes}->{TREASURE});
}

sub randomXY {
	my $self = shift;
	my $x = int rand($self->size);
	my $y = int rand($self->size);
	return ($x, $y);
}

sub emptyField {
	my $self = shift;
	my ($x, $y);
	do {
		($x, $y) = $self->randomXY;
	} while($self->elem($x, $y) != $self->{_fieldTypes}->{EMPTY});
	return ($x, $y);
}

sub dprint {
	my $self = shift;
	my $size = $self->size;
	for my $y (0..($size - 1)) {
		for my $x (0..($size - 1)) {
			print $self->elem($x, $y)." ";
		}
		print "\n";
	}
}

sub copyToArray {
	my $self = shift;
	my $fog = shift;

	my $copy = [];
	for my $i (0..($self->{_size} - 1)) {
		my $array = [];
		for my $j (0..($self->{_size} - 1)) {
			if($fog->elem($i, $j) != $self->{_fieldTypes}->{FOG}) {
				push(@$array, $self->elem($i, $j));
			}
			else {
				push(@$array, $self->{_fieldTypes}->{FOG});
			}
		}
		push(@$copy, $array);
	}
	return $copy;
}

sub getMinesCountAround {
	my $self = shift;
	my ($x, $y) = (shift, shift);
	my $size = $self->{_size};
	my $mine = $self->{_fieldTypes}->{MINE};
	my $count = 0;
	
	my $by = ($y - 1) % $size;
	my $ty = ($y + 1) % $size;
	my $lx = ($x - 1) % $size;
	my $rx = ($x + 1) % $size;

	++$count if($self->elem($x, $ty) == $mine);
	++$count if($self->elem($rx, $ty) == $mine);
	++$count if($self->elem($rx, $y) == $mine);
	++$count if($self->elem($rx, $by) == $mine);
	++$count if($self->elem($x, $by) == $mine);
	++$count if($self->elem($lx, $by) == $mine);
	++$count if($self->elem($lx, $y) == $mine);
	++$count if($self->elem($lx, $ty) == $mine);

	return $count;
}

1;